///////////////////////////////////////////////////////////////////////////////
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// Author: Bo Zhang
///////////////////////////////////////////////////////////////////////////////
#ifndef REAXFFLAMMPS
#define REAXFFLAMMPS

#include <iostream>
#include <string>
#include "mpi.h"

// LAMMPS include files
#include "atom.h"
#include "atom_vec.h"
#include "comm.h"
#include "domain.h"
#include "error.h"
#include "input.h"
#include "lammps.h"
#include "library.h"
#include "memory.h"
#include "pointers.h"

using namespace LAMMPS_NS;

//////////////////////////////////////////////////////////// 
// struct ExchangeInfo
////////////////////////////////////////////////////////////
struct ExchangeInfo
{
    int me;
    LAMMPS* lmp;
    double** model_atom_coord;
    int* model_atom_type;
    size_t mol_id_model;
    int natoms_per_molecule;
};

/// get model molecule types and positions
/// @param ptr: ExchangeInfo type pointer, will be casted inside
/// @throw if no model molecule is found
void get_model_molecule(void* ptr)
{
    ExchangeInfo *info = (ExchangeInfo *) ptr;
    int counter = 0;
    for(size_t i = 0; i < info->lmp->atom->nlocal; i ++)
        if( info->lmp->atom->molecule[i] == info->mol_id_model )
            counter ++;
    MPI_Allreduce(&counter, &info->natoms_per_molecule, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    info->lmp->memory->create(info->model_atom_coord, info->natoms_per_molecule, 3, "model_atom_coord"); // atomic coordinate
    info->model_atom_type = new int [info->natoms_per_molecule]; // type
     
    if (info->natoms_per_molecule == 0)
        info->lmp->error->all(FLERR, "could not find any atoms in the specified gas molecule");
    double **buf_coord;
    info->lmp->memory->create(buf_coord, info->natoms_per_molecule, 3, "buf_coord");
    int *buf_type = new int [info->natoms_per_molecule],
        *counts = new int [info->lmp->comm->nprocs],
        *counts3 = new int [info->lmp->comm->nprocs],
        *displacements = new int [info->lmp->comm->nprocs],
        *displacements3 = new int [info->lmp->comm->nprocs];
    counter = 0;
    for(int iproc = 0; iproc < info->lmp->comm->nprocs; iproc ++)
    {
        if( info->lmp->comm->me == iproc )
        {
            for(size_t i = 0; i < info->lmp->atom->nlocal; i ++)
            {
                if( info->lmp->atom->molecule[i] == info->mol_id_model )
                {
                    buf_coord[counter][0] = info->lmp->atom->x[i][0];
                    buf_coord[counter][1] = info->lmp->atom->x[i][1];
                    buf_coord[counter][2] = info->lmp->atom->x[i][2];
                    buf_type[counter ++ ] = info->lmp->atom->type[i];
                }
            }
        }
        counts[iproc] = counter;
        MPI_Bcast(&counts[iproc], 1, MPI_INT, iproc, MPI_COMM_WORLD);
        displacements[iproc] = 0;
        for(int i = 0; i < iproc; i ++)
            displacements[iproc] += counts[i];
        counts3[iproc] = counts[iproc] * 3;
        displacements3[iproc] = displacements[iproc] * 3;
    }
    MPI_Allgatherv(buf_coord[0], counter * 3, MPI_DOUBLE, info->model_atom_coord[0], counts3, displacements3, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Allgatherv(buf_type, counter, MPI_INT, info->model_atom_type, counts, displacements, MPI_INT, MPI_COMM_WORLD);
    info->lmp->memory->destroy(buf_coord);
    delete [] buf_type;
    delete [] counts;
    delete [] counts3;
    delete [] displacements;
    delete [] displacements3;
}

/// delete one molecule which must be model molecule type
/// @param ptr: ExchangeInfo type pointer, will be casted inside
/// @param mol_id_deleted: molecule id of the molecule will be deleted
void delete_molecule(void* ptr, size_t mol_id_deleted)
{
    ExchangeInfo* info = (ExchangeInfo *) ptr;
    int i = 0;
    while(i < info->lmp->atom->nlocal)
    {
        if( info->lmp->atom->molecule[i] == mol_id_deleted )
        {
            info->lmp->atom->avec->copy(info->lmp->atom->nlocal - 1, i, 1);
            info->lmp->atom->nlocal --;
        }
        else
            i ++;
    }
    info->lmp->atom->natoms -= info->natoms_per_molecule;
    info->lmp->atom->map_init();
    info->lmp->atom->nghost = 0;
    info->lmp->comm->borders();
}

/// insert one molecule which is model molecule type
/// @param ptr: ExchangeInfo type pointer, will be casted inside
/// @param mol_id_inserted: molecule id of molecule inserted
void insert_molecule(void* ptr, size_t* mol_id_inserted)
{
    ExchangeInfo* info = (ExchangeInfo *) ptr;
    bool *proc_flags = new bool [info->natoms_per_molecule];
    for(size_t i = 0; i < info->natoms_per_molecule; i ++)
    {
        double temp[3];
        temp[0] = info->model_atom_coord[i][0];
        temp[1] = info->model_atom_coord[i][1];
        temp[2] = info->model_atom_coord[i][2];
        info->lmp->domain->remap(temp);
        proc_flags[i] = false;
        if (temp[0] >= info->lmp->domain->sublo[0] && temp[0] < info->lmp->domain->subhi[0] &&
            temp[1] >= info->lmp->domain->sublo[1] && temp[1] < info->lmp->domain->subhi[1] &&
            temp[2] >= info->lmp->domain->sublo[2] && temp[2] < info->lmp->domain->subhi[2]) 
            proc_flags[i] = true;
    }
    int maxmol = 0;
    for (int i = 0; i < info->lmp->atom->nlocal; i++)
        maxmol = MAX(maxmol, info->lmp->atom->molecule[i]);
    MPI_Allreduce(&maxmol, mol_id_inserted, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
    *mol_id_inserted += 1;
    for(size_t i = 0; i < info->natoms_per_molecule; i ++)
    {
        int success = 0, success_all = 0;
        if( proc_flags[i] )
        {
            info->lmp->atom->avec->create_atom(info->model_atom_type[i], info->model_atom_coord[i]);
            info->lmp->atom->v[info->lmp->atom->nlocal - 1][0] = 0;
            info->lmp->atom->v[info->lmp->atom->nlocal - 1][1] = 0;
            info->lmp->atom->v[info->lmp->atom->nlocal - 1][2] = 0;
            info->lmp->atom->molecule[info->lmp->atom->nlocal - 1] = *mol_id_inserted;
            success = 1;
        }
        MPI_Allreduce(&success,&success_all,1,MPI_INT,MPI_MAX, MPI_COMM_WORLD);
        if ( success_all == 0 )
            info->lmp->error->all(FLERR, "inserting molecule");
    }
    if(info->lmp->atom->tag_enable)
    {
        info->lmp->atom->natoms += info->natoms_per_molecule;
        info->lmp->atom->tag_extend();
    }
    info->lmp->atom->map_init();
    info->lmp->atom->nghost = 0;
    info->lmp->comm->borders();
    delete [] proc_flags;
}

int main(int argc, char** argv)
{
    // setup MPI
    MPI_Init(&argc, &argv);
    MPI_Comm comm = MPI_COMM_WORLD;

    int me,nprocs;
    MPI_Comm_rank(comm,&me);
    MPI_Comm_size(comm,&nprocs);

    // setup LAMMPS, use all of cores availble
    LAMMPS *lmp = new LAMMPS(0, 0, MPI_COMM_WORLD);

    if (argc >= 2)
        lmp->input->file(argv[1]);
    else
        lmp->error->all(FLERR, "no input");

    // make info avaiable to callback function
    ExchangeInfo info;
    info.me = me;
    info.lmp = lmp;
    info.mol_id_model = 150;
    info.natoms_per_molecule = 0;
    info.model_atom_coord = 0;
    info.model_atom_type = 0;

    get_model_molecule(&info);

    if( me == 0 )
    {
        std::cout << info.natoms_per_molecule << std::endl;
        for(int i = 0; i < info.natoms_per_molecule; i ++)
            std::cout << info.model_atom_type[i] << " " << info.model_atom_coord[i][0] << " " << info.model_atom_coord[i][1] << " " << info.model_atom_coord[i][2] << std::endl;
    }
    lmp->input->one("run 0");
    size_t mol_id_deleted = 0,
           mol_id_inserted = info.mol_id_model,
           n_cycle = 0;
    if(argc >= 3)
        n_cycle = atoi(argv[2]);
    double *e_interaction = new double [n_cycle];
    for(size_t i = 0; i < n_cycle; i ++)
    {
        e_interaction[i] = *(double*)lammps_extract_variable(lmp, "pe", 0);
        // delete molecule
        mol_id_deleted = mol_id_inserted;
        delete_molecule(&info, mol_id_deleted);
        lmp->input->one("run 0");
        e_interaction[i] -= *(double*)lammps_extract_variable(lmp, "pe", 0);
        // insert molecule 
        insert_molecule(&info, &mol_id_inserted);
        lmp->input->one("run 0");
    }
    int natoms = static_cast<int> (lmp->atom->natoms);
    double *pos = new double[3 * natoms];
    lammps_gather_atoms(lmp, "x", 1, 3, pos);
    if( me == 0 )
    {
        std::cout << "finished " << n_cycle << std::endl;
        std::cout << "mol id = " << mol_id_inserted << std::endl;
        for(int i = 0; i < n_cycle; i ++)
            std::cout << "interaction energy = " << e_interaction[i] << std::endl;
        for(int i = 0; i < 3; i ++)
        {
            std::cout << pos[(natoms - 3) * 3 + i * 3] << " " << pos[(natoms - 3) * 3 + i * 3 + 1] << " " << pos[(natoms - 3) * 3 + i * 3 + 2] << std::endl;
        }
    }
    delete [] e_interaction;
    delete [] pos;

    // clean up
    delete lmp;
    if( info.model_atom_coord )
        lmp->memory->destroy(info.model_atom_coord);
    if( info.model_atom_type )
        delete [] info.model_atom_type;
    MPI_Finalize();
}

#endif
